# String To Card Type

convert the credit card number to automatic types of credit card.

**supported credit card types**

MasterCard, Visa, Discover, American Express, Diners, JCB

**usage**
```
"5105 1051 0510 5100".toCardType();
```
> MASTER

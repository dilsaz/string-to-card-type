/* eslint no-extend-native: ["error", { "exceptions": ["String"] }] */
String.prototype.toCardType =  function () {
    const cardNumber = (this || "").trim().replace(/[^0-9]/g, '');
    const cardTypes  = {
        VISA        : "^(4)",
        MASTER      : "^(5|(2[2-7]2))",
        AMEX        : "^(34|37)",
        DINERS_CLUB : "^(30|36|38|39)",
        JCB         : "^(35[2-8])",
        DISCOVER    : "^(6[0-5])"
    };

    let result = "";
    
    for ( let type in cardTypes ){
        const reg = new RegExp( cardTypes[type] );
        if ( cardNumber.match( reg ) !== null )
            result = type;
    }

    return result;
};
